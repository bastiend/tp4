

$(document).ready(function () {
    /***Variables globales de prix */
    var prixtot = 0;
    var prix_pizza = 0;
    var prix_pate = 0;
    var prix_extra = 0;
    var slices = 1;


    /*** Hover au dessus des recettes */
    $('.pizza-type label').hover(
        function () {
            $(this).find(".description").show();
        },
        function () {
            $(this).find(".description").hide();
        }
    );


    /*** Bouton etape suivante */
    $('.next-step').click(function () {
        $(this).remove();
        $(".infos-client").show();
    });


    /*** Bouton ajouter une ligne d'adresse */
    $('.add').click(function () {
        $(this).before('<br><input type="text"/>');
    });


    /*** Bouton Terminer */
    $('.done').click(function () {
        var prenom = $('.infos-client .type:first-child input').val();
        $('.typography-row').remove();
        $('.stick-right').remove();
        $(".container").append("Merci à toi " + prenom + ", ta commande arrive dans 15min ! <br><br>");
    });


    /***Nombre de parts  */
    $('.nb-parts input').on('keyup', function () {
        $(".pizza-pict").remove();
        var pizza = $('<span class="pizza-pict"></span>');

        slices = +$(this).val();

        for (i = 0; i < slices / 6; i++) {
            $(this).after(pizza.clone().addClass('pizza-6'));
        }
        if (slices % 6 != 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-' + slices % 6);
        affichage();
    });

    /*** Clic sur un type de pizza */
    $(".pizza-type input[type='radio'][name='type']").click(function () {
        prix_pizza = $(this).attr('data-price');
        prix_pizza /= 6;
        affichage();
    });


    /*** Clic sur un type de pate */
    $(".type input[type='radio'][name='pate']").click(function () {
        prix_pate = $(this).attr('data-price');
        prix_pate /= 6;
        affichage();
    });


    /*** Clic sur les differents extras */
    $(this).change(function () {
        prix_extra = 0;
        $(" input[name='extra']:checked").each(function (i, elem) {
            prix_extra += ($(elem).data("price")) / 6;
        });
        affichage();
    });


    /*** Calcul du prix total grace aux variables globales */
    function affichage() {
        prixtot = (prix_pizza + prix_pate + prix_extra) * slices;
        prixtot = Math.round((prixtot * 100)) / 100; //Arrondi a 0.01

        $('.stick-right p').text(prixtot + " € ");
    }
});
